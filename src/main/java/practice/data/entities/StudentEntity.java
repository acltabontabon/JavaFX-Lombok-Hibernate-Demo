package practice.data.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "students")
public class StudentEntity {

    @Id
    @Column(name = "student_id")
    @Getter @Setter
    private String studentId;

    @Column(name = "name")
    @Getter @Setter
    private String name;

    @Column(name = "course")
    @Getter @Setter
    private String course;

    @OneToOne(mappedBy = "student")
    @Getter @Setter
    private StudentAccountEntity studentAccount;

}
