package practice.data.dao.interfaces;

import org.hibernate.Session;
import org.hibernate.criterion.Criterion;

import java.util.List;


public interface DAO<T, ID> {

    public T findById(ID id);

    public List<T> findAll();

    public List<T> findByCriteria(Criterion...criterions);

    public void save(T entity);

    public void delete(T entity);

    public void flush();

    public void clear();

    public void setSession(Session session);
}
