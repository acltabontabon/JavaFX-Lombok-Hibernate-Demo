package practice.data.dao.interfaces;

import practice.data.entities.StudentEntity;


public interface StudentDao extends DAO<StudentEntity, String> {

    public StudentEntity findByCourse(String course);

}
