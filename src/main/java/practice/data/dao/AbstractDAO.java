package practice.data.dao;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import practice.data.HibernateUtil;
import practice.data.dao.interfaces.DAO;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;


public abstract class AbstractDAO<T, ID extends Serializable> implements DAO<T, ID> {

    @Getter
    private Class<T> persistentClass;

    @Setter
    private Session session;

    public AbstractDAO() {
        this.persistentClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    public Session getSession() {
        if (session == null) {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
        }

        return session;
    }

    @Override
    public T findById(ID id) {
        return (T) this.getSession().load(getPersistentClass(), id);
    }

    @Override
    public List<T> findAll() {
        return this.findByCriteria();
    }

    @Override
    public List<T> findByCriteria(Criterion... criterions) {
        Criteria criteria = this.session.createCriteria(this.getPersistentClass());

        for (Criterion criterion: criterions) {
            criteria.add(criterion);
        }

        return (List<T>) criteria.list();
    }

    @Override
    public void save(T entity) {
        this.getSession().saveOrUpdate(entity);
    }

    @Override
    public void delete(T entity) {
        this.getSession().delete(entity);
    }

    @Override
    public void flush() {
        this.getSession().flush();
    }

    @Override
    public void clear() {
        this.getSession().clear();
    }
}
