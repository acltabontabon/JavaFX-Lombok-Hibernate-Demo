package practice.app;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.hibernate.Session;
import org.hibernate.Transaction;
import practice.data.HibernateUtil;
import practice.data.dao.implementations.StudentDaoImpl;
import practice.data.dao.interfaces.StudentDao;
import practice.data.entities.StudentAccountEntity;
import practice.data.entities.StudentEntity;

import java.util.List;


public class MainApp extends Application {



    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent parent = FXMLLoader.load(getClass().getResource("/fxml/MainApp.fxml"));
        Scene scene = new Scene(parent);
        primaryStage.setScene(scene);
        primaryStage.setOnCloseRequest(evt -> {
            try {
                System.out.println("Closing...");
                HibernateUtil.getSessionFactory().close();
            } catch (Exception e) {
                System.out.println("Failed to close hibernate...");
                System.out.println("Error Message: " + e.getLocalizedMessage());

                Platform.exit();
            }
        });
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }


}
