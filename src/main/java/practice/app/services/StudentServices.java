package practice.app.services;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import lombok.Cleanup;
import lombok.Setter;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import practice.data.HibernateUtil;
import practice.data.dao.implementations.StudentDaoImpl;
import practice.data.dao.interfaces.StudentDao;
import practice.data.entities.StudentEntity;


public class StudentServices extends Service<ObservableList<StudentEntity>> {

    @Setter
    private Criterion criterion;

    @Override
    protected Task<ObservableList<StudentEntity>> createTask() {
        return new QueryTask();
    }

    private class QueryTask extends Task<ObservableList<StudentEntity>> {

        @Override
        protected ObservableList<StudentEntity> call() throws Exception {
            @Cleanup Session session = HibernateUtil.getSessionFactory().openSession();

            ObservableList<StudentEntity> lst;
            StudentDao dao = new StudentDaoImpl();
            dao.setSession(session);

            if (criterion != null) {
                lst = FXCollections.observableArrayList(dao.findByCriteria(criterion));
                criterion = null;
            }
            else
                lst = FXCollections.observableArrayList(dao.findByCriteria());


            return lst;
        }
    }
}
