package practice.app.controller;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import lombok.Cleanup;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import practice.app.services.StudentServices;
import practice.data.HibernateUtil;
import practice.data.dao.implementations.StudentDaoImpl;
import practice.data.dao.interfaces.StudentDao;
import practice.data.entities.StudentAccountEntity;
import practice.data.entities.StudentEntity;

import java.net.URL;
import java.util.ResourceBundle;


public class MainAppController implements Initializable {

    @FXML private TextField txtStudentNumber, txtName, txtCourse, txtSearch;
    @FXML private TableView tblViewStudents;
    @FXML private TableColumn colStudentNumber, colName, colCourse;

    private static MainAppController instance;

    private StudentServices studentService;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        instance = this;
        studentService = new StudentServices();

        colStudentNumber.setCellValueFactory(new PropertyValueFactory<StudentEntity, String>("studentId"));
        colName.setCellValueFactory(new PropertyValueFactory<StudentEntity, String>("name"));
        colCourse.setCellValueFactory(new PropertyValueFactory<StudentEntity, String>("course"));

        tblViewStudents.itemsProperty().bind(studentService.valueProperty());
        studentService.start();
    }

    @FXML
    private void save(ActionEvent evt) {
        @Cleanup Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        try {
            StudentDao studentDao = new StudentDaoImpl();
            studentDao.setSession(session);

            StudentEntity student = new StudentEntity();
            StudentAccountEntity acct = new StudentAccountEntity();

            student.setName(txtName.getText());
            student.setStudentId(txtStudentNumber.getText());
            student.setCourse(txtCourse.getText());
            student.setStudentAccount(acct);

            acct.setPassword("password");
            acct.setStudent(student);

            studentDao.save(student);
            transaction.commit();

            clearInputFields();
        } catch (Exception e) {
            System.out.println("Something went wrong...");
            e.printStackTrace();
            transaction.rollback();
        }

        studentService.restart();
    }

    @FXML
    private void delete(ActionEvent evt) {
        StudentEntity student = (StudentEntity) tblViewStudents.getSelectionModel().getSelectedItem();

        if (student != null) {
            @Cleanup Session session = HibernateUtil.getSessionFactory().openSession();
            Transaction transaction = session.beginTransaction();

            StudentDao dao = new StudentDaoImpl();
            dao.setSession(session);

            dao.delete(student);
            transaction.commit();

            studentService.restart();
        }
    }

    @FXML
    private void selectEntryToUpdate(ActionEvent evt) {
        StudentEntity student = (StudentEntity) tblViewStudents.getSelectionModel().getSelectedItem();

        if (student != null) {
            txtStudentNumber.setText(student.getStudentId());
            txtName.setText(student.getName());
            txtCourse.setText(student.getCourse());
        }
    }

    @FXML
    private void search(ActionEvent evt) {
        studentService.setCriterion(Restrictions.like("name", "%" + txtSearch.getText() + "%"));
        studentService.restart();
    }

    private void clearInputFields() {
        txtName.clear();
        txtCourse.clear();
        txtStudentNumber.clear();
    }
}
